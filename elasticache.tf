// Cluster
resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = "${var.name}"
  replication_group_description = "Elasticache Redis ${var.name} ${var.family}"
  number_cache_clusters         = "${var.number_cache_clusters}"
  node_type                     = "${var.node_type}"
  automatic_failover_enabled    = "${var.automatic_failover_enabled}"
  engine_version                = "${var.engine_version}"
  parameter_group_name          = "${aws_elasticache_parameter_group.redis.id}"
  port                          = "${var.port}"
  subnet_group_name             = "${aws_elasticache_subnet_group.redis.name}"
  security_group_ids            = ["${aws_security_group.redis.id}"]
  maintenance_window            = "${var.maintenance_window}"
  notification_topic_arn        = "${var.notification_topic_arn}"
  apply_immediately             = "${var.apply_immediately}"
  tags                          = "${var.tags}"

  #   # Incompatibility with single nodes
  #   snapshot_window               = "${var.snapshot_window}"
  #   snapshot_retention_limit      = "${var.snapshot_retention_limit}"
}

#output "elasticache_replication_group" {
#  value = {
#    id                             = "${aws_elasticache_replication_group.redis.id}"
#    primary_endpoint_address       = "${aws_elasticache_replication_group.redis.primary_endpoint_address}"
#    configuration_endpoint_address = "${aws_elasticache_replication_group.redis.configuration_endpoint_address}"
#  }
#}
