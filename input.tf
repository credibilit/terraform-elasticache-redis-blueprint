variable "account" {
  description = "The AWS account number ID"
}

variable "name" {
  description = "A uniq name for ElastiCache Redis components"
}

variable "family" {
  description = "The family of the ElastiCache parameter group"
  default     = "redis3.2"
}

variable "security_groups" {
  type        = "list"
  description = "List of security groups to add permission"
  default     = []
}

variable "port" {
  description = "The port number on which each of the cache nodes will accept connections. Default: 6379"
  default     = 6379
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnet_ids" {
  type        = "list"
  description = "List of VPC Subnet IDs for the cache subnet group"
}

variable "node_type" {
  description = "The compute and memory capacity of the nodes in the node group"
  default     = "cache.m3.medium"
}

variable "number_cache_clusters" {
  description = "The number of cache clusters this replication group will have. If Multi-AZ is enabled , the value of this parameter must be at least 2"
  default     = 2
}

variable "automatic_failover_enabled" {
  description = "Specifies whether a read-only replica will be automatically promoted to read/write primary if the existing primary fails. Default: true"
  default     = true
}

variable "engine_version" {
  description = "The version number of the cache engine to be used for the cache clusters in this replication group. Default: 3.2.4"
  default     = "3.2.4"
}

variable "maintenance_window" {
  description = "Specifies the weekly time range for when maintenance on the cache cluster is performed. The format is ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period"
  default     = "sun:04:00-sun:06:00"
}

variable "notification_topic_arn" {
  description = "An Amazon Resource Name (ARN) of an SNS topic to send ElastiCache notifications to"
  default     = ""
}

variable "snapshot_window" {
  description = "The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster"
  default     = "06:00-07:00"
}

variable "snapshot_retention_limit" {
  description = "The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them"
  default     = 5
}

variable "apply_immediately" {
  description = "Specifies whether any modifications are applied immediately, or during the next maintenance window. Default: true"
  default     = true
}

variable "tags" {
  type    = "map"
  default = {}
}

variable "parameters" {
  type    = "list"
  default = []
}
