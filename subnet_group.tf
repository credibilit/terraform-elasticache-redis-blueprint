// Subnet Group
resource "aws_elasticache_subnet_group" "redis" {
  name       = "${var.name}"
  subnet_ids = ["${var.subnet_ids}"]
}

output "elasticache_subnet_group" {
  value = {
    name       = "${aws_elasticache_subnet_group.redis.name}"
    subnet_ids = "${aws_elasticache_subnet_group.redis.subnet_ids}"
  }
}
