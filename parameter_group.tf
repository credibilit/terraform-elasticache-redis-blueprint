// Parameter Group
resource "aws_elasticache_parameter_group" "redis" {
  name   = "${var.name}"
  family = "${var.family}"

  # parameter = "${var.parameter}"

  parameter = "${var.parameters}"
}

output "elasticache_parameter_group" {
  value = {
    id = "${aws_elasticache_parameter_group.redis.id}"
  }
}
