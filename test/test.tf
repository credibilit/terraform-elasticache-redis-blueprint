// VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "elasticache-test"
  }
}

// Private subnets
resource "aws_subnet" "cache_a" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "cache-a"
  }
}

resource "aws_subnet" "cache_b" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags {
    Name = "cache-b"
  }
}

// Variables
variable "account" {}

// Elasticache Redis with HA
module "test" {
  source  = "../"
  account = "${var.account}"

  name   = "redis-test-acme"
  vpc_id = "${aws_vpc.default.id}"

  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}",
  ]

  parameters = [
    {
      name  = "appendonly"
      value = "yes"
    },
    {
      name  = "close-on-slave-write"
      value = "yes"
    },
  ]

  tags = {
    Foo = "bar"
  }
}

output "test" {
  value = "${module.test.elasticache_replication_group}"
}

// Elasticache Redis single node
module "test_single" {
  source  = "../"
  account = "${var.account}"

  name                       = "redis-test-acme-sing"
  number_cache_clusters      = 1
  maintenance_window         = "sun:05:00-sun:06:00"
  node_type                  = "cache.t2.micro"
  automatic_failover_enabled = false
  family                     = "redis2.8"
  engine_version             = "2.8.24"

  vpc_id = "${aws_vpc.default.id}"

  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}",
  ]

  # tags = {
  #   Foo = "bar"
  # }

  # parameter = {
  #   "name"  = "appendonly"
  #   "value" = "yes"
  # }

  # parameter = {
  #   name  = "close-on-slave-write"
  #   value = "yes"
  # }
}

output "test_single" {
  value = "${module.test_single.elasticache_replication_group}"
}
