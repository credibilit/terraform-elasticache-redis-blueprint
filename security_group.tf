resource "aws_security_group" "redis" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix = "elasticache-redis-${var.name}"
  description = "Elasticache Redis ${var.name}"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name = "elasticache-redis-${var.name}"
  }
}

resource "aws_security_group_rule" "redis_egress" {
  security_group_id = "${aws_security_group.redis.id}"
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

output "security_group" {
  value = {
    id   = "${aws_security_group.redis.id}"
    name = "${aws_security_group.redis.name}"
  }
}
