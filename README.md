Elasticache Redis Blueprint
===========================

This Terraform module is a blueprint to elasticache redis cluster with HA

# Use

To create a bucket with this module you need to insert the following piece of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-elasticache-redis-blueprint.git?ref=<VERSION>"
  account = "${var.account}"

  name            = "redis-test-acme"
  security_groups = ["${aws_security_group.default.id}"]
  vpc_id          = "${aws_vpc.default.id}"
  subnet_ids = [
    "${aws_subnet.cache_a.id}",
    "${aws_subnet.cache_b.id}"
  ]
  tags = {
    Foo = "bar"
  }
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.


## Input parameters

The following parameters are used on this module:

- `account`: The AWS account number ID
- `name`: A uniq name for ElastiCache Redis components
- `vpc_id`: The VPC ID
- `subnet_ids`: List of VPC Subnet IDs for the cache subnet group

- `family`: The family of the ElastiCache parameter group
- `security_groups`: List of security groups to add permission
- `port`: The port number on which each of the cache nodes will accept connections. Default: 6379
- `node_type`: The compute and memory capacity of the nodes in the node group
- `number_cache_clusters`: The number of cache clusters this replication group will have. If Multi-AZ is enabled , the value of this parameter must be at least 2
- `automatic_failover_enabled`: Specifies whether a read-only replica will be automatically promoted to read/write primary if the existing primary fails. Default: true
- `engine_version`: The version number of the cache engine to be used for the cache clusters in this replication group. Default: 3.2.4
- `maintenance_window`: Specifies the weekly time range for when maintenance on the cache cluster is performed. The format is ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period
- `notification_topic_arn`: An Amazon Resource Name (ARN) of an SNS topic to send ElastiCache notifications to
- `snapshot_window`: The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster
- `snapshot_retention_limit`: The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them
- `apply_immediately`: Specifies whether any modifications are applied immediately, or during the next maintenance window. Default: true
- `tags`: Map of tags

## Output parameters

This are the outputs exposed by this module.

- `security_group`
- `test`
- `test_single`
- `elasticache_replication_group`
- `elasticache_subnet_group`
- `elasticache_parameter_group`
